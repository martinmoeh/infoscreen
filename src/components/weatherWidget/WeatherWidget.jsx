import React, { Component } from 'react';
import WidgetContainer from '../widgetContainer';

class WeatherWidget extends Component {

	constructor(props) {
		super(props);

		this.state = {
			weather: '',
			time: ''
		};

		setInterval(this._updateTime.bind(this), 1000);
	}

	_updateTime() {
		const now = new Date();

		let timeStr = now.getDate();


		this.setState({
			time: timeStr
		});
	}

	render() {
		return (
			<WidgetContainer>
				<div className="weatherWidget">
					{this.state.time}
					This is a weather widget
				</div>
			</WidgetContainer>
		)
	}
};

export default WeatherWidget;