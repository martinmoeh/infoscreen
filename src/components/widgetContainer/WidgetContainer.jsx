import React from 'react';
import './style.css';

const WidgetContainer = (props) => {
	console.log(props.children);
	return (
		<div className="widgetContainer">
			This is a widgetContainer
			{props.children}
		</div>
	)
};

export default WidgetContainer;