import React from 'react';
import WeatherWidget from './components/weatherWidget';

export default () => (
	<div className="app">
		<WeatherWidget/>
	</div>
);